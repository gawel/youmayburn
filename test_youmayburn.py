import youmayburn
import dateutil.tz
import datetime
import pytest

UTC = dateutil.tz.gettz('UTC')
PARIS = dateutil.tz.gettz('Europe/Paris')


@pytest.mark.parametrize('utcday,utchour,out,weekend', [
    # GMT+2
    (27, 5, 1, 0),
    (27, 6, 0, 0),
    (27, 17, 0, 0),
    (27, 18, 1, 0),

    # weekend
    (28, 10, 1, 1),
    (29, 10, 1, 1),

    # GMT+1
    (30, 6, 1, 0),
    (30, 7, 0, 0),
    (30, 18, 0, 0),
    (30, 19, 1, 0),
])
def test_process_commit(utcday, utchour, out, weekend):
    detector = youmayburn.Detector(since=datetime.datetime.now())
    dt = datetime.datetime(2023, 10, utcday, utchour, 1, tzinfo=UTC)
    res = detector.process_commit(dt.timestamp(), "Gael")
    assert res['out'] == out
    assert len(res['weekends']) == weekend

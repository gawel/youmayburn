# You may burn

Try to detect burnouts from git commits

## Dependencies

Install the following using apt:

```
apt install python3-dateutil
```

## Usage

See script's help:

```
$ ./youmayburn.py --help
```

This will show a resume since 2023-01-01 for all the repo of entr'ouvert gitea:

```
$ ./youmayburn.py --since 2023-1-1 $(curl -s https://git.entrouvert.org/api/v1/orgs/entrouvert/repos/\?limit\=50 https://git.entrouvert.org/api/v1/orgs/entrouvert/repos/\?limit\=50\&page\={1..8} |jq -r ".[] | select(.archived == false) | .clone_url")
```

# Testing

You can run tests using pytest:

```
$ pytest -v
```

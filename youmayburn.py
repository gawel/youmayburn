#!/usr/bin/env python3
import argparse
import datetime
import dateutil.tz
from operator import itemgetter
import logging
import os
import textwrap
import subprocess


class Detector:

    def __init__(self, default_timezone='Europe/Paris',
                 since=None, day_start=8, day_end=20):
        self.default_timezone = dateutil.tz.gettz(default_timezone)
        self.day_start = day_start
        self.day_end = day_end
        self.data = {}
        if not isinstance(since, datetime.datetime):
            raise ValueError('since argument must be a valid datetime object')
        self.since = since
        self.repositories = os.path.join(os.getcwd(), '.youmayburn')
        if not os.path.isdir(self.repositories):
            os.makedirs(self.repositories)

    def process_commit(self, timestamp, author):
        """analyse if the commit is done outside working hours and store result"""
        ad = datetime.datetime.fromtimestamp(timestamp, tz=self.default_timezone)
        cdata = self.data.setdefault(
            author.lower(),
            {'total': 0, 'out': 0, 'weeks': set(), 'weekends': set()}
        )
        week = ad.isocalendar()[:2]
        cdata['total'] += 1
        if ad.isoweekday() > 5:
            logging.debug(f'week-end {ad}')
            cdata['out'] += 1
            cdata['weekends'].add(week)
            cdata['weeks'].add(week)
        else:
            start = datetime.datetime(
                ad.year, ad.month, ad.day, self.day_start,
                tzinfo=self.default_timezone)
            end = datetime.datetime(
                ad.year, ad.month, ad.day, self.day_end,
                tzinfo=self.default_timezone)
            if not (start < ad < end):
                logging.debug(f'outside working hours {ad}')
                cdata['out'] += 1
                cdata['weeks'].add(week)
        return cdata

    def process_repository_logs(self, directory):
        """extract data from git log and process commits"""
        logging.debug('Processing %s', directory)
        cmd = [
            "git", f"--git-dir={directory}/.git",
            "log", "--date=unix", "--format=%ad;%an",
        ]
        cmd.append(f"--since={self.since.strftime('%Y-%m-%d')}")
        logging.debug(' '.join(cmd))

        stdout = subprocess.check_output(cmd, encoding='utf8')
        for line in stdout.strip().split('\n'):
            if not line:
                continue
            logging.debug(line)
            ad, an = line.split(';')
            self.process_commit(ad, an)

    def process_repository(self, repository):
        """checkout the repository and process git logs"""
        name = repository.split('/')[-1]
        directory = os.path.join(self.repositories, name)
        if not os.path.isdir(directory):
            subprocess.check_call([
                'git', 'clone', '-q', repository, directory
            ])
        self.process_repository_logs(directory)

    def show_report(self):
        now = datetime.datetime.now()
        since = self.since
        weeks = int((now - since).days / 7)
        print(f"Report since {since.strftime('%Y-%m-%d')} ({weeks} weeks)")
        print("===========================================")
        data = [
            dict(cdata, commiter=commiter, weeks_amount=len(cdata['weeks']))
            for commiter, cdata in self.data.items()
            if cdata['out']
        ]
        data = sorted(data, key=itemgetter('weeks_amount'), reverse=True)
        for cdata in data:
            print(textwrap.dedent(f"""
            {cdata["commiter"]}
            -------------------------------------------
            {cdata["weeks_amount"]} weeks with overtime
            {len(cdata["weekends"])} week-ends overtime
            {cdata["total"]} commits
            {cdata["out"] / cdata["total"] * 100:.2f}% overtime commits
            """))


def main():
    now = datetime.datetime.now()
    now = datetime.datetime(now.year, now.month, now.day)
    since = now - datetime.timedelta(days=365)

    parser = argparse.ArgumentParser()
    parser.add_argument('repository', nargs="+", help="remote git repository")
    parser.add_argument('--timezone', default='Europe/Paris',
                        help="default timezone. Default: Europe/Paris")
    parser.add_argument('-s', '--day-start', type=int, default=8,
                        help="start hour. Default: 8")
    parser.add_argument('-e', '--day-end', type=int, default=20,
                        help="end hour. Default: 20")
    parser.add_argument('--since',
                        default=None,
                        help=(
                            "date formated as YYYY-MM-DD. "
                            f"Default: {since.strftime('%Y-%m-%d')}"
                        ))
    parser.add_argument('--debug', action="store_true", default=False,
                        help="enable debug mode")
    args = parser.parse_args()

    if args.since:
        try:
            since = datetime.datetime(*[int(n) for n in args.since.split('-')])
        except ValueError:
            parser.error(
                f"Invalide since: {args.since}. "
                "Use a valid date with YYYY-MM-DD format.")

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.INFO)

    logging.debug(args)

    detector = Detector(
        default_timezone=args.timezone,
        since=since,
        day_start=args.day_start,
        day_end=args.day_end,
    )

    for repository in args.repository:
        detector.process_repository(repository)

    detector.show_report()


if __name__ == '__main__':
    main()

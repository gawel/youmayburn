# A propos

Ce projet est un simple exercice visant à "détecter" les burnouts.

Le but est de connaitre la part des commit éffectués en dehors des heures de travail.

# Problématique

Il faut extraire les données d'une repository git. git log est hautement maléable.
Après quelque recherches dans l'aide, on s'appercoit que l'on ne peut extraire
que le strict necessaire pour notre besoin. Mon choix a été d'extraire le nom,
le mail et la date associée au commit de l'auteur. La date est extraite au
format unix. Ce qui est probablement ce qui demande le moins de CPU.

Le résultat est conscit et ne necessite probablement pas de buffer pour traiter
les résultats. Même sur des repository conséquent.

Viens ensuite le problème des horaires de travail. Autant il est trivial de
détecter les commits fait un week-end, autant il faut tenir compte, dans notre
timezone, de l'heure d'hiver et de l'heure d'été pour définir des horaires
acceptable de travail. Après quelques recherche, datutil, packagé pour debian,
fourni des utilitaires relativement simple pour définir une timezone. Mon choix
a été de rajouter l'offset de la timezone à la date UTC obtenue depuis le
timestamp unix.

La dernière problématique est "qui". L'auteur et sont mail sont défini
arbitrairement dans la configuration locale du commiteur. Ainsi, la même
personne peut utiliser des noms et emails différent. J'ai opté pour utiliser le
champs nom comme clé. A première vue, une même personne pourra utiliser
plusieurs emails mais aura toujours le même nom. Cette solution semble générer
moins de doublons que d'utiliser l'email comme clé.



# TODO

- améliorer la gestion de la clé "nom", en l'état, il peut y avoir des doublons
  lié à des accents: Gael vs Gaël.

- afficher les données des X derniers mois, par semaine, afin de détecter
  plus facilement les périodes de forte activités. Si elles sont ponctuelles,
  c'est un coup de bourre acceptable. Si elles sont systématique, la personne est
  en train de se "cramer" [done]

- tracker toutes les branches. il est possible que la personne passe ses nuits
  sur une fetaure request ou autre, dissocié dans une branche

- supporter plus d'un repository [done]

- ajouter un fichier de configuration permettant d'associer une timezone à un auteur
